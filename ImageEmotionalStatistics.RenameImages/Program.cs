﻿using System;
using System.Drawing;
using System.IO;

namespace ImageEmotionalStatistics.RenameImages
{
    class Program
    {
        private const string PathToImages = @"C:\Users\Борис\Desktop\Course Work\images\Db2014";
        private const string PathToImagesMinified = @"Db2014Minified";

        private const int MaxSize = 400;

        static void Main()
        {
            string[] imagesPathes = Directory.GetFiles(PathToImages);
            int i = 0;
            foreach (var imagePath in imagesPathes)
            {
                ++i;
                Bitmap image = new Bitmap(imagePath);
                var h = image.Height;
                var w = image.Width;
                if (h > MaxSize)
                {
                    w = (int) (w * (MaxSize * 1.0 / h));
                    h = MaxSize;
                }
                if (w > MaxSize)
                {
                    h = (int)(h * (MaxSize * 1.0) / w);
                    w = MaxSize;
                }
                Bitmap imageNew = new Bitmap(image, w, h);

                imageNew.Save(String.Format(@"{0}\{1}\{2}.jpg", PathToImages, PathToImagesMinified, i.ToString("D4")));
                Console.WriteLine("Обработано {0} файлов", i);
            }

            Console.WriteLine("---------------------\nОбработка завершена. Было обработано {0} файлов", i);
        }
    }
}
