﻿using System;
using System.IO;
using ImageEmotionalStatistics.DataLayer.DataModels;
using DbImage = ImageEmotionalStatistics.DataLayer.DataModels.Image;

namespace ImageEmotionalStatistics.ImportImagesToDb
{
    class Program
    {
        private const string PathToImages = @"C:\Users\Борис\Desktop\Course Work\images\Db2014\Db2014Minified";
        static void Main()
        {
            ImageEmotionalEntities imageEntities = new ImageEmotionalEntities();

            string[] imagesPathes = Directory.GetFiles(PathToImages);
            for (int i = 0; i < imagesPathes.Length; i++)
            {
                var imagePath = imagesPathes[i];
                byte[] imageBytes = File.ReadAllBytes(imagePath);

                DbImage imageModel = new DbImage
                    {
                        Name = Path.GetFileNameWithoutExtension(imagePath),
                        Data = imageBytes
                    };
                imageEntities.Images.Add(imageModel);

                if ((i + 1) % 40 == 0)
                {
                    imageEntities.SaveChanges();
                }

                Console.WriteLine("Обработано {0} файлов", i);
            }
            imageEntities.SaveChanges();
            Console.WriteLine("------------------\nСохранение в базу завершено");
        }
    }
}
