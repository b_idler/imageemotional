﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ImageEmotionalStatistics.Consts;
using ImageEmotionalStatistics.DataLayer.DataModels;
using ImageEmotionalStatistics.Helpers;

namespace ImageEmotionalStatistics.ResultsAnalyze
{
    class Program
    {
        static void Main()
        {
            var imageEntities = new ImageEmotionalEntities();

            Action<string> log = Console.WriteLine;
            using (var writer = new StreamWriter("resultAssessments.txt"))
            {
                log += writer.WriteLine;

                List<IGrouping<string, UserAnswer>> userAnswers = imageEntities.UserAnswers.GroupBy(item => item.Image.Name).ToList();
                foreach (var imageAnswer in userAnswers)
                {
                    var imageId = imageAnswer.Key;
                    log(String.Format("Image {0}", imageId));
                    decimal positive = 0;
                    decimal quite = 0;
                    decimal male = 0;
                    decimal live = 0;
                    foreach (var userAnswer in imageAnswer)
                    {
                        positive += userAnswer.PositiveNegative;
                        quite += userAnswer.QuiteMove;
                        male += userAnswer.MaleFemale;
                        live += userAnswer.LiveRobotic;

                        //log(String.Format("  Pos={0}, Quite={1}, Male={2}, Live={3}", userAnswer.PositiveNegative, userAnswer.QuiteMove, userAnswer.MaleFemale, userAnswer.LiveRobotic));
                    }
                    int cntImages = imageAnswer.Count();
                    var res = new StringBuilder("  ");
                    
                    positive /= cntImages;
                    if (positive > (decimal) 0.5)
                        res.Append(PositiveNegativeEnum.Positive.Description());
                    else if (positive < (decimal) 0.5)
                        res.Append(PositiveNegativeEnum.Negative.Description());
                    else
                        res.Append(PositiveNegativeEnum.DontKnow.Description());
                    res.Append(" ");

                    quite /= cntImages;
                    if (quite > (decimal)0.5)
                        res.Append(QuiteMoveEnum.Quite.Description());
                    else if (quite < (decimal)0.5)
                        res.Append(QuiteMoveEnum.Move.Description());
                    else
                        res.Append(QuiteMoveEnum.DontKnow.Description());
                    res.Append(" ");

                    male /= cntImages;
                    if (male > (decimal)0.5)
                        res.Append(MaleFemaleEnum.Male.Description());
                    else if (male < (decimal)0.5)
                        res.Append(MaleFemaleEnum.Female.Description());
                    else
                        res.Append(MaleFemaleEnum.DontKnow.Description());
                    res.Append(" ");

                    live /= cntImages;
                    if (live > (decimal)0.5)
                        res.Append(LiveRoboticEnum.Live.Description());
                    else if (live < (decimal)0.5)
                        res.Append(LiveRoboticEnum.Robotic.Description());
                    else
                        res.Append(LiveRoboticEnum.DontKnow.Description());

                    log(res.ToString());
                    log(Environment.NewLine);
                }
            }
        }
    }
}
