﻿using System.ComponentModel;

namespace ImageEmotionalStatistics.Consts
{
    public enum PositiveNegativeEnum
    {
        [Description("Позитивная")]
        Positive = 0,

        [Description("Не знаю")]
        DontKnow = 1,

        [Description("Негативная")]
        Negative = 2
    }
}