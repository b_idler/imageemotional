﻿using System.ComponentModel;

namespace ImageEmotionalStatistics.Consts
{
    public enum MaleFemaleEnum
    {
        [Description("Мужественная")]
        Male = 0,

        [Description("Не знаю")]
        DontKnow = 1,

        [Description("Женственная")]
        Female = 2
    }
}