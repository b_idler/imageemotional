﻿using System.ComponentModel;

namespace ImageEmotionalStatistics.Consts
{
    public enum LiveRoboticEnum
    {
        [Description("Живая")]
        Live = 0,

        [Description("Не знаю")]
        DontKnow = 1,

        [Description("Роботизированная")]
        Robotic = 2
    }
}