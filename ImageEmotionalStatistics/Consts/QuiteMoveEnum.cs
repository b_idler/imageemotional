﻿using System.ComponentModel;

namespace ImageEmotionalStatistics.Consts
{
    public enum QuiteMoveEnum
    {
        [Description("Спокойная")]
        Quite = 0,

        [Description("Не знаю")]
        DontKnow = 1,

        [Description("Динамическая")]
        Move = 2
    }
}