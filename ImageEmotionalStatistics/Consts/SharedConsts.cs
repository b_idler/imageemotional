﻿namespace ImageEmotionalStatistics.Consts
{
    public static class SharedConsts
    {
        public const int CountSemanticProperties = 4;
        public const int CountMaxUsersInRaiting = 5;
    }
}