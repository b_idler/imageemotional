﻿using System.Collections.Generic;

namespace ImageEmotionalStatistics.Models.Shared
{
    public class UserRaiting
    {
        public List<UserInfo> Users { get; set; }
        public UserInfo AuthorInfo { get; set; }
    }

    public class UserInfo
    {
        public string Name { get; set; }
        public string ComputerName { get; set; }
        public int CountAnswers { get; set; }
    }
}