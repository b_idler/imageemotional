﻿using System;
using System.Collections.Generic;
using ImageEmotionalStatistics.Consts;

namespace ImageEmotionalStatistics.Models.ImageTestModels
{
    public class ImageTestModel
    {
        public int ImageNumber { get; set; }
        public int ImageIdInDb { get; set; }
        public int MaxImageNumber { get; set; }
        public byte[] ImageData { get; set; }

        public List<SemanticPropertyType> SemanticProperties { get; set; }

        public ImageTestModel() { }
        public void InitializeSemanticProperties()
        {
            SemanticProperties = new List<SemanticPropertyType>
            {
                new SemanticPropertyType
                {
                    TypeOfEnum = typeof (PositiveNegativeEnum)
                },
                new SemanticPropertyType
                {
                    TypeOfEnum = typeof (QuiteMoveEnum)
                },
                new SemanticPropertyType
                {
                    TypeOfEnum = typeof (MaleFemaleEnum)
                },
                new SemanticPropertyType
                {
                    TypeOfEnum = typeof (LiveRoboticEnum)
                }
            };
        }
    }

    public class SemanticPropertyType
    {
        public int Value { get; set; }
        public Type TypeOfEnum { get; set; }
    }
}