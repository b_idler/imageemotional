﻿using System;
using System.Linq;
using System.Web.Mvc;
using ImageEmotionalStatistics.Consts;
using ImageEmotionalStatistics.DataLayer.DataModels;
using ImageEmotionalStatistics.Models.ImageTestModels;

namespace ImageEmotionalStatistics.Controllers
{
    public class ImageTestController : BaseController
    {
        public ActionResult Index()
        {
            int totalImageNumberInDb = ImageEntities.Images.Count();
            int imageNumberFromDb = new Random((int)DateTime.Now.Ticks).Next(totalImageNumberInDb - 1) + 1;
            int imageNumber;

            if (Session["imageNumber"] == null)
            {
                imageNumber = 1;
                Session["imageNumber"] = 1;
            }
            else
            {
                var sessionImageNumber = Session["imageNumber"];
                imageNumber = sessionImageNumber is int ? (int) sessionImageNumber : 1;
            }

            var idOfSelectedImage = ImageEntities.Images.Select(item => item.Id).ToArray()[imageNumberFromDb];

            ImageTestModel model = new ImageTestModel
                {
                    ImageNumber = imageNumber,
                    ImageData = ImageEntities.Images.Single(item => item.Id == idOfSelectedImage).Data,
                    ImageIdInDb = idOfSelectedImage
                };
            model.InitializeSemanticProperties();
            return View(model);
        }

        [HttpPost]
        public ActionResult Answer(ImageTestModel model)
        {
            int maxIdInDb = ImageEntities.Images.Max(item => item.Id);
            int minIdInDb = ImageEntities.Images.Min(item => item.Id);
            if (model.ImageIdInDb < minIdInDb || model.ImageIdInDb > maxIdInDb || model.SemanticProperties == null || model.SemanticProperties.Count != SharedConsts.CountSemanticProperties)
            {
                return View("BadPage");
            }

            var image = ImageEntities.Images.SingleOrDefault(item => item.Id == model.ImageIdInDb);
            if (image == null)
            {
                return View("BadPage");
            }

            UserAnswer userAnswer = new UserAnswer
                {
                    PositiveNegative = ((decimal) model.SemanticProperties[0].Value) / 2,
                    QuiteMove = ((decimal) model.SemanticProperties[1].Value) / 2,
                    MaleFemale = ((decimal) model.SemanticProperties[2].Value) / 2,
                    LiveRobotic = ((decimal) model.SemanticProperties[3].Value) / 2,
                    CreateDateTime = DateTime.Now,
                    User = GetComputerName()
                };
            image.UserAnswers.Add(userAnswer);
            ImageEntities.SaveChanges();

            var sessionImageNumber = Session["imageNumber"];
            var imageNumber = sessionImageNumber is int ? (int)sessionImageNumber : 1;
            Session["imageNumber"] = imageNumber + 1;

            return RedirectToAction("Index", "ImageTest");
        }
    }
}
