﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ImageEmotionalStatistics.DataLayer.DataModels;
using ImageEmotionalStatistics.Models.Shared;

namespace ImageEmotionalStatistics.Controllers
{
    public class BaseController : Controller
    {
        protected readonly ImageEmotionalEntities ImageEntities = new ImageEmotionalEntities();
        private readonly object _lockObj = new object();

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            ViewData["Users"] = GetUsersRaiting();
        }

        /// <summary>
        /// Круто было бы использовать регистрацию и идентифицировать пользователя по логину\паролю, но вряд ли кто-то будет регистрироваться
        /// Также можно сохранять какие-то данные на клиенте в кэш, и потом идентифицировать по ним. Но они могут устаревать и т.п.
        /// </summary>
        protected string GetComputerName()
        {
            return Request.ServerVariables["REMOTE_ADDR"];
        }

        private UserRaiting GetUsersRaiting()
        {
            var userRaiting = new UserRaiting();
            var userInfoes = ImageEntities.UserAnswers.Select(item => new UserInfo {ComputerName = item.User, CountAnswers = 1});
            var groupUsers = userInfoes.GroupBy(item => item.ComputerName, info => info.CountAnswers);
            var users = groupUsers.Select(item => new UserInfo {ComputerName = item.Key, CountAnswers = item.Sum()}).OrderByDescending(item => item.CountAnswers)
                                  .ToList();
            userRaiting.Users = users;
            userRaiting.AuthorInfo = new UserInfo
                {
                    ComputerName = GetComputerName()
                };
            var authorUser = users.FirstOrDefault(item => item.ComputerName == userRaiting.AuthorInfo.ComputerName);
            if (authorUser != null)
            {
                userRaiting.AuthorInfo.CountAnswers = authorUser.CountAnswers;
            }

            ReplaceRealNamesInUserRating(userRaiting);

            return userRaiting;
        }

        private void ReplaceRealNamesInUserRating(UserRaiting userRating)
        {
            var authorNames = ImageEntities.AuthorNames.ToList();
            var authorDictionary = authorNames.ToDictionary(item => item.ComputerName, item => item.Name);
            foreach (UserInfo user in userRating.Users.Where(item => !string.IsNullOrEmpty(item.ComputerName)))
            {
                user.Name = authorDictionary.ContainsKey(user.ComputerName)
                                ? authorDictionary[user.ComputerName]
                                : user.ComputerName;
            }

            userRating.AuthorInfo.Name = authorDictionary.ContainsKey(userRating.AuthorInfo.ComputerName)
                                             ? authorDictionary[userRating.AuthorInfo.ComputerName]
                                             : null;
        }

        public JsonResult ChangeAuthorName(string name)
        {
            lock (_lockObj)
            {
                var computerName = GetComputerName();
                var authorLink = ImageEntities.AuthorNames.FirstOrDefault(item => item.ComputerName == computerName);
                if (authorLink != null)
                {
                    authorLink.Name = name;
                }
                else
                {
                    ImageEntities.AuthorNames.Add(new AuthorName
                        {
                            ComputerName = computerName,
                            Name = name
                        });
                }

                ImageEntities.SaveChanges();

                return Json(new {correct = true});
            }
        }
    }
}