﻿using System.Web.Mvc;

namespace ImageEmotionalStatistics.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
